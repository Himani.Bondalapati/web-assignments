//reverse

const reverse = arr => {
  const result = [];
  for (let i = arr.length - 1; i >= 0; i--) result.push(arr[i]);
  return result;
};
console.log(reverse([1, 2, 3, 4]));

//map function

const even1 = x => x % 2 === 0;
const map1 = (f, arr) => {
  const result = [];
  for (const i of arr) result.push(f(i));

  return result;
};
console.log(map1(even1, [1, 2, 3, 4]));

//Filter function

const odd = n => n % 2 !== 0;
const filter = (f, arr) => {
  const result = [];
  for (const e of arr) if (f(e)) result.push(e);

  return result;
};
console.log(filter(odd, [1, 2, 3, 4, 5]));

//Reduce function

const mul = (x, y) => x * y;
const reduce = (f, arr) => {
  let result = [1];
  for (const i of arr) result = f(result, i);

  return result;
};
console.log(reduce(mul, [3, 4]));

//takewhile function

const odd1 = n => n % 2 !== 0;
const takewhile = (f, arr) => {
  const result = [];
  for (const i of arr) if (f(i)) result.push(i);
  return result;
};
console.log(takewhile(odd1, [2, 3, 4, 5]));


//Dropwhile function
const evend = n =>{
  return n % 2 === 0;
};
const dropwhile = (f, arr) =>{
  for(let i=0;i<arr.length;i++){
  if(f(arr[i])===true){


    if(f(arr[i+1])===false){
      return arr.slice(i+1);
    }
  }
  else{return arr;}
  }
};
console.log(dropwhile(evend,[2,4,6,3,7,4]));
