const summ = arr => {
  var sum = 0;
  for (var i = 0; i < arr.length; i++) sum += arr[i];
  return sum;
};
console.log(summ([2, 3]));

const index = (arr, val) => {
  for (let i = 0; i < arr.length; i++) if (i == val) return arr[i];
};
console.log(index(["a", "s", "f", "u"], 3));

const squareall = arr => {
  const result = [];
  for (var i = 0; i <= arr.length; i++) {
    result.push(arr[i] * arr[i]);
  }
  return result;
};
console.log(squareall([1, 2]));

const factorial = n => {
  let fact = 1;
  for (let i = 1; i <= n; i++) fact = fact * i;
  return fact;
};
factorial(4);

const concat1 = (arr, arr1) => {
  for (var j = 0; j < arr1.length; j++) arr.push(arr1[j]);
  return arr;
};
console.log(concat1([1, 2, 3], [4, 5]));

const concat = (arr, arr1) => {
  const result = [];
  for (var i = 0; i < arr.length; i++) result.push(arr[i]);
  for (var j = 0; j < arr1.length; j++) result.push(arr1[j]);

  return result;
};

console.log(concat([1, 2, 3], [4, 5]));

const take = (n, arr) => {
  const result = [];
  for (var i = 0; i < n; i++) result.push(arr[i]);
  return result;
};
console.log(take(2, [1, 2, 3, 4, 5, 6]));

const prime = num => {
  let i = 2;
  var count = 0;
  for (let i = 2; i < num; i++) if (num % i === 0) count++;
  if (count === 0) return true;
  else return false;
};
console.log(prime(13));

const drop = (n, arr) => {
  const result = [];
  for (var i = n; i < arr.length; i++) result.push(arr[i]);
  return result;
};
console.log(drop(2, [1, 2, 3, 4, 5, 6]));

const reverse = arr => {
  const result = [];
  for (var i = arr.length - 1; i >= 0; i--) result.push(arr[i]);
  return result;
};
console.log(reverse([1, 2, 3, 4]));

const perfect = num => {
  var sum = 0,
    rem = 0;

  for (let i = 0; i <= num - 1; i++) {
    rem = num % i;
    if (rem === 0) sum = sum + i;
  }
  if (num == sum) return true;
  else return false;
};
console.log(perfect(6));

