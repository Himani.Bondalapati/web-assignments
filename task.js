const power = (x, y) => {
  var result = 1;
  for (let i = 1; i <= y; i++) {
    result = result * x;
    if (y == 0) return 1;
  }
  return result;
};
console.log(power(2, 8));

const repeat = (n, x) => {
  const result = [];
  for (let i = 0; i <= n; i++) result.push(x);
  return result;
};
console.log(repeat(5, "*"));

const maxvalue = arr => {
  var max = 0;
  for (var i = 0; i <= arr.length; i++) if (arr[i] > max) max = arr[i];
  return max;
};

console.log(maxvalue([6, 7, 8]));

const even = arr => {
  const result = [];
  for (var i = 0; i < arr.length; i++)
    if (arr[i] % 2 === 0) result.push(arr[i]);
  return result;
};
console.log(even([2, 3, 4, 5, 6]));

const summ = arr => {
  var sum = 0;
  for (var i = 0; i < arr.length; i++) sum += arr[i];
  return sum;
};
console.log(summ([2, 3]));

const index = (arr, val) => {
  for (let i = 0; i < arr.length; i++) if (i == val) return arr[i];
};
console.log(index(["a", "s", "f", "u"], 3));

