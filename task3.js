//split 
const split = (arr, index) => {
  const first = [];
  for (let i = 0; i < index; i++) first.push(arr[i]);
  const second = [];
  for (let i = index; i < arr.length; i += 1) second.push(arr[i]);
  return [first, second];
};
console.log(split([1, 2, 3, 4, 5, 6], 3));
//pluck
const prop = { x: 1, y: 2 };
const pluck = (obj, props) => {
  var result = {};
  for (const prop of props) {
    result[prop] = obj[prop];
  }
  return result;
};
console.log(pluck(prop, ["x", "y"]));
//clone
const clone = obj => {
  const result = {};
  for (let i in obj) result[i] = obj[i];
  return result;
};
console.log(clone({ x: 3, y: 2 }));
//assoc
const assoc = (k, v, obj) => {
  const result = clone(obj);
  result[k] = v;
  return result;
};
console.log(assoc("c", 3, { a: 2, b: 4 }));
//dissoc
const dissoc = (k, obj) => {
  const result = [];
  for (const i in obj) if (i !== k) result[i] = obj[i];
  return result;
};
console.log(dissoc("b", { a: 2, b: 4, c: 3 }));
//merge
const merge = (ob1, ob2) => {
  const result = {};
  for (const k in ob1) result[k] = ob1[k];
  for (const k in ob2) result[k] = ob2[k];
  return result;
};
console.log(merge({ a: 1, b: 2 }, { c: 3, d: 4 }));
//invert
const invert = obj => {
  const result = {};
  for (let i = 1; i < obj; i++) result.push(obj[i]);

  return obj;
};
console.log(invert({ alice: 'first', jake: 'second' }));




