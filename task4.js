//sorting
function sorting(arr) {
  for (let i = 0; i < arr.length - 1; i++)
    if (arr[i] > arr[i + 1]) return false;

  return true;
}
console.log(sorting([2, 3, 4, 5]));

//every
const eve = x => x % 2 === 0;
const map = (f, arr) => {
  let lst = true;
  for (const i of arr) lst = lst && f(i);

  return lst;
};
console.log(map(eve, [2, 7, 6]));

const evenn = n => n % 2 === 0;
const any = (f, arr) => {
  let a = false;
  for (const l of arr) a = a || f(l);

  return a;
};

console.log(any(evenn, [7, 3, 4, 5]));


